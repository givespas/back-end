const { Model, DataTypes } = require('sequelize');
const sequelize = require('../database/db.js');

class User extends Model {}
User.init({
    name: DataTypes.TEXT,
    email: DataTypes.TEXT,
    password: DataTypes.TEXT,
    status: DataTypes.TEXT
}, {
    sequelize,
    modelName: "users"
});

module.exports = User;