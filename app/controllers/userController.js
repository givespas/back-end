const User = require('../models/user');

// GET USER ALL
const getUserAll = async (req, res) => {

    User.findAll().then(users => {
        res.json(users);
    });

};

// GET USER FOR ID
const getUserId = async (req, res) => {

    const id = parseInt(req.params.id);
    User.findByPk(id).then(result => {
        res.json(result);
    })
};

// CREATE USER
const createUser = async (req, res) => {

    User.create({
        name: req.body.name,
        email: req.body.email,
        password: req.body.password,
        status: req.body.status
    }).then(result => {
        res.json(result);
    })

};

// UPDATE USER
const updateUser = async (req, res) => {
    
    let id = parseInt(req.params.id);
    User.update({
        id: req.body.id,
        name: req.body.name,
        email: req.body.email,
        password: req.body.password,
        status: req.body.status
    }, {
        where: {
            id: id
        }
    }).then(result => {
        res.json(result);
    });
};

// DELETE USER
const destroyUser = async (req, res) => {
    
    const id = parseInt(req.params.id);
    User.destroy({
        where: {
            id: id
        }
    });
    res.json(`User ${id} deleted Successfully`);
};

module.exports = {
    getUserAll,
    getUserId,
    createUser,
    updateUser,
    destroyUser
};