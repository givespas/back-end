const { Router } = require('express');
const router = Router();

const { getUserAll, getUserId, createUser, updateUser, destroyUser }
        = require('../controllers/userController');

// api users
router.get('/users', getUserAll);
router.get('/users/:id', getUserId);
router.post('/users', createUser);
router.put('/users/:id', updateUser)
router.delete('/users/:id', destroyUser);

module.exports = router;