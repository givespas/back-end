CREATE DATABASE test;

\l

\c test;

CREATE TABLE users (
    id SERIAL PRIMARY KEY,
    name VARCHAR(40),
    email TEXT,
    password TEXT,
    status TEXT
);

INSERT INTO users (name, email)
    VALUES ('joe', 'joe@ibm.com', '1234', 'activo');


select * from users;