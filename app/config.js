require('dotenv').config()

module.exports = {
    database: {
        host: process.env.DB_HOST,
        user: process.env.DB_USER,
        pass: process.env.DB_PASS,
        db: process.env.DB_NAME,
        port: process.env.DB_PORT
    }
}