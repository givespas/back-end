const express = require('express');
const app = express();
const sequelize = require('./app/database/db.js');
require('dotenv').config()

// middlewares
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// Rutas
app.get('/', function (req, res) {
    res.json("Welcome");
});

app.use(require('./app/routes/api'));

// Serve
app.listen(process.env.SERVE_PORT || 3000, function () {
    console.log('Server on port', process.env.SERVE_PORT);

    sequelize.sync({ force: true }).then(() => {
        console.log("Conection db");
    }).catch(error => {
        console.log('Error db', error);
    })

});

