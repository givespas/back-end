# Test técnico Nodejs
## Cargo BackEnd - Duración 4 hrs

## install:

    - git clone https://givespas@bitbucket.org/givespas/back-end.git
    - npm install
    - Open postgres terminal
    - Command in terminal (CREATE DATABASE test;)
    - create file .env
    - copy data of .env-example and config your db

## Run:

    - npm run dev